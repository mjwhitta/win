module gitlab.com/mjwhitta/win

go 1.17

require (
	gitlab.com/mjwhitta/errors v1.0.0
	gitlab.com/mjwhitta/pathname v1.2.1
	golang.org/x/sys v0.0.0-20220804214406-8e32c043e418
)
